# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* First of all, create a dedicated folder on your Hard Disk for Git Repositories. 
* Download and Open SmartGit from http://www.syntevo.com/smartgit/
* Once Downloaded, open it and select Repository -> Clone
* On the dialogue that opens, select "Remote Git or SVN repository", and on the URL field, paste "https://bitbucket.org/nekoraptor/ld35"
* When prompted, select "Git"
* Follow the instructions on-screen, and select the directory you previously created on locally.
* Once everything is done, click on "Pull" to obtain the latest version. Make sure you do this every time before pushing anything.
* Work on the designated folder, and you'll notice that everything you change will appear on the "files" window. 
* When you're ready to send your changes, click on the "commit" button, and type a message that summarizes your changes. MAKE THIS CLEAR FOR EVERYONE.
* Once you have committed, PULL the repository to get the latest version (and avoid file conflicts) and then PUSH your changes.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Bob or Alex
* Other community or team contact