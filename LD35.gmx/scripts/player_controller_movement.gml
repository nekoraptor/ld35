///player_controller_movement(maxspeed);
//spd = argument0;

var button_sword = gamepad_button_check(0,gp_face2);
var button_shield = gamepad_button_check(0,gp_shoulderr);
var button_ranged = gamepad_button_check(0,gp_shoulderrb);
var button_dash = gamepad_button_check(0,gp_face1);



gamepad_set_axis_deadzone(controller,0.2)
var haxis = gamepad_axis_value(controller, gp_axislh);
var vaxis = gamepad_axis_value(controller, gp_axislv);
direction = point_direction(0, 0, haxis, vaxis);
speed = point_distance(0 ,0, haxis, vaxis) * speed_current;
//image_angle = direction;
image_speed = maxspeed/50;


if (button_sword){
    form = 1;
}

if (button_shield){
    form = 2;
}

if (button_ranged){
    form = 3;
}

if (button_dash){
    form = 4; //dash
}

//show_debug_message("form = " + string(form));

switch(form){

    case 0: //normal
        sprite_index=spr_player1_idle;
        
        image_speed=0.5;
        break;
        
    case 1: //sword
        sprite_index=spr_sword_red;
        
        image_speed=0.5;
        break;
        
    case 2: //shield
        sprite_index=spr_red_shield;
        
        image_speed=0.5;
        break;
        
    case 3: //shooter
    
        break;
        
    case 4: //dash
    
        break;

}

if (speed=0){
    image_angle = last_direction;
    sprite_index = idle_sprite;
}else{
    image_angle = direction;
    last_direction = direction;
    sprite_index = moving_sprite;
}
