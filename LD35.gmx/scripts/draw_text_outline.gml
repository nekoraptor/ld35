///draw_text_outline(x, y, text, textColor, outlineColor, scale)

var xx=argument0;
var yy=argument1;
var text=argument2;
var tcolor=argument3;
var ocolor=argument4;
var scale=argument5;

//Draw the outline
draw_set_color(ocolor);
draw_text_transformed(xx-1, yy, text, scale, scale, 1);
draw_text_transformed(xx+1, yy, text, scale, scale, 1);
draw_text_transformed(xx, yy-1, text, scale, scale, 1);
draw_text_transformed(xx, yy+1, text, scale, scale, 1);

//Draw the text
draw_set_color(tcolor);
draw_text_transformed(xx, yy, text, scale, scale, 1);

