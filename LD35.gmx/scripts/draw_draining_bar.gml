///draw_draining_bar(x, y, width, outlinecolor, filledcolor, draincolor, maxvalue, currentvalue, drainvalue, drainspeed)

///////////////////////////////////
//TO USE THIS SCRIPT:
// * Create three different attributes on the create event of the object:
// * (Example: Max_HP, Current_HP, Draining_HP)
// * Max_HP = 100;
// * Current_HP = 100;
// * Draining_HP = currentvalue;
//////////////////////////////////

var xx = argument0;
var yy = argument1;
var barWidth = argument2;
var outlinecolor = argument3;
var filledcolor = argument4;
var draincolor = argument5;
var maxvalue = argument6;
var currentvalue = argument7;
var drainvalue = argument8;
var drainspeed = argument9; //Optimal value ~200. Smaller means faster.

    //Draws outline
    draw_set_color(outlinecolor)
    draw_rectangle(xx,yy, xx+barWidth, yy+6, true)
    //Calculates the draining            
    drainvalue += min(abs(currentvalue-drainvalue) , maxvalue/drainspeed )* sign(currentvalue-drainvalue)
    //Draws the draining bar
    draw_set_color(draincolor)    
    draw_rectangle(xx,yy,x+(drainvalue/maxvalue)*barWidth, yy+6,false)
    //Draws the healthbar over the draining bar
    draw_set_color(filledcolor)
    draw_rectangle(xx, yy, x+(currentvalue/maxvalue)*barWidth, yy+6, false);

        
